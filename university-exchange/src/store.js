import Vue from 'vue'
import Vuex from 'vuex'
import books from './modules/books'
import student from './modules/student'
import VuexPersistence from "vuex-persist";

Vue.use(Vuex)

const vuexLocal = new VuexPersistence({
  storage: window.localStorage
})

export default new Vuex.Store({
  modules: {
    books,
    student
  },
  plugins: [vuexLocal.plugin]
})
