import Vue from 'vue'

export async function loginStudent({commit}) {
  try{
    const {data} = await Vue.axios({
      method: "POST",
      url: '/login/access-token',
      headers: {
        "Access-Control-Allow-Origin": "*",
        "Content-Type": "application/x-www-form-urlencoded"
      },
      data: 'grant_type=&username=cdspinosa%40gmail.com&password=escu1905&scope=&client_id=&client_secret='
    })
    commit('setStudent', data)
  } catch (e){
    commit('Login Error', e.message)
  } finally {
    console.log("request login end.")
  }
}
