import Vue from 'vue'

export async function getBooks({commit}) {
  try{
    const {data} = await Vue.axios({
      url: '/books',
      headers: {
        "Access-Control-Allow-Origin": "*"
      }
    })
    commit('setBooks', data)
  } catch (e) {
    commit('Books Error', e.message)
  } finally {
    console.log("request books end.")
  }
}
