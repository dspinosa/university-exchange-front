import Vue from "vue"
import Router from 'vue-router'
import Books from './views/books'


Vue.use(Router)

export default new Router({
  mode: 'history',
  base: '/',
  fallback: true,
  routes: [
    {
      path: '/books',
      name: 'books',
      component: Books
    }
  ]
})
